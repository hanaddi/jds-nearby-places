-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 09, 2021 at 11:46 AM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `JCC2021_JDS`
--

-- --------------------------------------------------------

--
-- Table structure for table `Kategori`
--

CREATE TABLE `Kategori` (
  `id_kategori` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LevelWilayah`
--

CREATE TABLE `LevelWilayah` (
  `id_levelwilayah` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Tempat`
--

CREATE TABLE `Tempat` (
  `id_tempat` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `kode_kelurahan` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Wilayah`
--

CREATE TABLE `Wilayah` (
  `id_wilayah` int(11) NOT NULL,
  `kode_kemendagri` varchar(20) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `id_levelwilayah` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Kategori`
--
ALTER TABLE `Kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `LevelWilayah`
--
ALTER TABLE `LevelWilayah`
  ADD PRIMARY KEY (`id_levelwilayah`);

--
-- Indexes for table `Tempat`
--
ALTER TABLE `Tempat`
  ADD PRIMARY KEY (`id_tempat`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `kode_kelurahan` (`kode_kelurahan`);

--
-- Indexes for table `Wilayah`
--
ALTER TABLE `Wilayah`
  ADD PRIMARY KEY (`id_wilayah`),
  ADD UNIQUE KEY `kode_kemendagri` (`kode_kemendagri`),
  ADD KEY `id_levelwilayah` (`id_levelwilayah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Kategori`
--
ALTER TABLE `Kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `LevelWilayah`
--
ALTER TABLE `LevelWilayah`
  MODIFY `id_levelwilayah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Tempat`
--
ALTER TABLE `Tempat`
  MODIFY `id_tempat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39754;
--
-- AUTO_INCREMENT for table `Wilayah`
--
ALTER TABLE `Wilayah`
  MODIFY `id_wilayah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6412;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Tempat`
--
ALTER TABLE `Tempat`
  ADD CONSTRAINT `Tempat_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `Kategori` (`id_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Tempat_ibfk_2` FOREIGN KEY (`kode_kelurahan`) REFERENCES `Wilayah` (`kode_kemendagri`) ON UPDATE CASCADE;

--
-- Constraints for table `Wilayah`
--
ALTER TABLE `Wilayah`
  ADD CONSTRAINT `Wilayah_ibfk_1` FOREIGN KEY (`id_levelwilayah`) REFERENCES `LevelWilayah` (`id_levelwilayah`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
