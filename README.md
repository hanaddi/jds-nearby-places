# Get Nearby Places
API pencarian tempat/point of interest di wilayah Jawa Barat

#### Keyboardist
- Nama  :Fat Han Nuraddin
- Kelas :React Native
- API base url :https://nearby-places.api.online.masuk.id
- Video demo :https://youtu.be/gsJ3Xn5hH58

#### Sumber data
- Daftar kabupaten/kota di Jawa Barat : https://opendata.jabarprov.go.id/id/dataset/kode-wilayah-dan-nama-wilayah-kota-kabupaten-di-jawa-barat
- Daftar kecamatan di Jawa Barat : https://opendata.jabarprov.go.id/id/dataset/kode-wilayah-dan-nama-wilayah-kecamatan-di-jawa-barat
- Daftar kelurahan/desa di Jawa Barat :  https://opendata.jabarprov.go.id/id/dataset/kode-wilayah-dan-nama-wilayah-desa-kelurahan-di-jawa-barat
- Validasi kode wilayah administrasi :  https://www.kemendagri.go.id/media/filemanager/2013/05/28/b/u/buku_induk_kode_data_dan_wilayah_2013.pdf
- Daftar point of interest di Jawa Barat : https://satupeta.jabarprov.go.id/web/#/map

#### Instalasi
- Letakkan file di root directory
- Buat MySQL database
- Import skema basis data `database_structure.sql`
- Import isi basis data `database_data.sql`
- Edit `db.php` sesuaikan host, username, dan password ke basis data yang sudah dibuat
- Semua request ditangani oleh `rute.php`