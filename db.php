<?php
$_db=new mysqli("localhost","root","pass","database");
if ($_db->connect_error) {
	die("Connection failed: " . $_db->connect_error);
}
$_db->set_charset('utf8mb4');

function _norm($s){
	global $_db;
	return $_db->real_escape_string($s);
}
?>
