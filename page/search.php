<?php
if($_SERVER["REQUEST_METHOD"]!='GET'){
	http_response_code(405);
	echo json_encode([
		'error' => "Method tidak dibolehkan",
	]);
	exit();
}

if(!is_get_valid(['latitude', 'longitude'])){
	http_response_code(400);
	echo json_encode([
		'error' => "Request salah",
	]);
	exit();
}

$response = [];
$latitude = floatval($_GET['latitude']);
$longitude = floatval($_GET['longitude']);
$category_id = isset($_GET['category_id'])?intval($_GET['category_id']):null;

$data = get_poi($latitude, $longitude, $category_id);
$response = [
	'data' => $data,
	'total' => count($data),
];
echo json_encode($response);



/**
* Mendapatkan Tempat/point of interest dari koordinat tertentu
* @param float $latitude latitude dari titik yang dicari
* @param float $longitude longitude dari titik yang dicari
* @param int/null $category_id filter kategori tempat
* @return array data tempat
*/
function get_poi($latitude, $longitude, $category_id=null){
	global $_db;
	$offside=0.045; //sekitar 5km
	$max_result = 500;
	$min_lat = $latitude - $offside;
	$max_lat = $latitude + $offside;
	$min_lon = $longitude - $offside;
	$max_lon = $longitude + $offside;

	//filter kategori
	$filter_category = "1=1";
	if(!is_null($category_id)){
		$filter_category = " Kategori.id_kategori=".($category_id);
	}
	
	$query = sprintf("SELECT id_tempat, Tempat.name, Kategori.name as kategori, Kategori.id_kategori, Tempat.latitude, Tempat.longitude, Kecamatan.name as kecamatan, Kota.name as kota FROM `Tempat`, `Kategori`, `Wilayah` as Kecamatan, Wilayah as Kota WHERE %s AND Tempat.latitude>=%s AND Tempat.latitude<=%s AND Tempat.longitude>=%s AND Tempat.longitude<=%s AND Tempat.id_kategori = Kategori.id_kategori AND substring_index(Tempat.kode_kelurahan,'.',3) = Kecamatan.kode_kemendagri AND substring_index(Tempat.kode_kelurahan,'.',2) = Kota.kode_kemendagri ORDER BY `id_tempat` ASC ",
		_norm($filter_category),
		_norm($min_lat),
		_norm($max_lat),
		_norm($min_lon),
		_norm($max_lon)
	);

	$query_result = $_db -> query($query);
	$result = [];
	while($row = mysqli_fetch_array($query_result,1)){
		$distance = haversineGreatCircleDistance($latitude, $longitude, floatval($row['latitude']), floatval($row['longitude']) );

		// tempat dalam radius 5km
		if($distance <= 5000){
			$row['distance'] = round($distance);
			$result[] = [
				'id' => intval($row['id_tempat']),
				'name' => $row['name'],
				'category_id' => intval($row['id_kategori']),
				'city_name' => $row['kota'],
				'district_name' => $row['kecamatan'],
				'latitude' => floatval($row['latitude']),
				'longitude' => floatval($row['longitude'])
			];
		}
		if(count($result)>=$max_result)break;
	}
	return $result;
}

/**
 * Calculates the great-circle distance between two points, with
 * the Haversine formula.
 * https://stackoverflow.com/a/14751773
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
	$latFrom = deg2rad($latitudeFrom);
	$lonFrom = deg2rad($longitudeFrom);
	$latTo = deg2rad($latitudeTo);
	$lonTo = deg2rad($longitudeTo);

	$latDelta = $latTo - $latFrom;
	$lonDelta = $lonTo - $lonFrom;

	$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
	return $angle * $earthRadius;
}

?>