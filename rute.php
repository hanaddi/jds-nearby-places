<?php
// session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
error_reporting(0);

require 'db.php';

$rute = substr($_SERVER["REQUEST_URI"], 1);


// generate $_GET
$req = [];
foreach (['?', '#'] as $v) {
	$r = explode( $v , $rute);
	$rute = $r[0];
	$req[$v] = null;
	if(isset($r[1])){
		$req[$v] = $r[1];
	}
}
$req['?'] = explode('&', $req['?']);
foreach ($req['?'] as $k => $v) {
	unset($req['?'][$k]);
	$v = explode('=', $v);
	if($v[0]=='')continue;
	if(!isset($v[1])){
		$v[1]=null;
	}
	$v[1] = urldecode($v[1]);
	$req['?'][$v[0]] = $v[1];
}
$_GET=$req['?'];

$login=null;
$myurl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$canonical = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}";


// JSON REQUEST
if($_SERVER['CONTENT_TYPE']=='application/json'){
	$_POST = json_decode(file_get_contents('php://input'), true);
}

if(0){}
// rute :/search
else if(prefix('search')){
	include 'page/search.php';
}
// home page
else if(prefix('')){
	header("Content-Type: text/plain; charset=utf-8");
	echo 'End point API Nearby Places';
}

/*ERROR N FRIENDS*/
else if(prefix('404') ) {
	header("Content-Type: text/html; charset=utf-8");
	include 'error/404.php';
}

else {
	header("Content-Type: text/html; charset=utf-8");
	include 'error/404.php';
}

///////////////////////////////


// test rute
function prefix($x,$p=0){
	global $rute, $canonical;
	$loc1 = explode('/', strtolower($rute.'/'));
	if(isset($loc1[$p]) && strtolower($x) == $loc1[$p] ){
		$canonical .="/".$loc1[$p];
		return true;
	}
	return false;
}

// cek apakah semua element dalam array $a ada di $_POST
function is_post_valid($a){
	foreach ($a as $v) {
		if(!isset($_POST[$v])){
			return false;
		}
	}
	return true;
}

// cek apakah semua element dalam array $a ada di $_GET
function is_get_valid($a){
	foreach ($a as $v) {
		if(!isset($_GET[$v])){
			return false;
		}
	}
	return true;
}

// mendapatkan rute uri case insensitive
function get_rute($p=0){
	global $rute;
	$loc1 = explode('/', strtolower($rute));
	return isset($loc1[$p]) ? $loc1[$p] : false;
}


// mendapatkan rute uri case sensitive
function get_ruteP($p=0){
	global $rute;
	$loc1 = explode('/', ($rute));
	return isset($loc1[$p]) ? $loc1[$p] : false;
}
?>